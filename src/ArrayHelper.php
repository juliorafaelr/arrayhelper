<?php namespace juliorafaelr\ArrayHelper;

class ArrayHelper {
	/**
	 * divide an array into defined partition numbers
	 *
	 * @param	array	$array
	 * @param	int		$p		number of partitions
	 *
	 * @return array
	 */

	public static function array_partition( array $array, int $p ): array {
		$i = 0;

		$new_array = array( );

		while( empty( $part = array_slice( $array, ( $p * $i ), $p ) ) === false ){
			$new_array[ ] = $part;

			$i++;
		}

		return $new_array;
	}

	/**
	 * flat all array's values to a single level array
	 *
	 * @param array $array
	 *
	 * @return array
	 */

	public static function array_flatten( array $array ): array {
		$flatten = array( );

		array_walk_recursive($array, function( $item ) use( &$flatten ) {
			$flatten[ ] = $item;
		});

		return $flatten;
	}

	/**
	 * sort Array by  Value's Length
	 *
	 * @param array $array
	 *
	 * @return array
	 */

	public static function sortArrayValueByLength( array $array ): array {
		uasort( $array, function ( $a, $b ) {
			return strlen( $b ) - strlen( $a );
		});

		return $array;
	}

	/**
	 * find all the values of the array present on the subject string
	 *
	 * @param array $array
	 * @param string $subject
	 * @param bool $ignoreSubStrings
	 *
	 * @return array
	 */

	public static function findArrayValuesOnString( array $array, string $subject, $ignoreSubStrings = false ): array {
		$count = 0;

		$array = self::sortArrayValueByLength( $array );

		$found = array( );

		foreach ( $array as $key => $value ) {
			if ( stripos( $subject, $value ) !== false ) {
				if ( $ignoreSubStrings ) {
					$subject = str_replace( $value, '', $subject, $count );
				}

				$found[ $key ][ 'value' ] = $value;

				$found[ $key ][ 'count' ] = $count;

				$count = 0;
			}
		}

		return $found;
	}

	/**
	 * create placeholders from a array set
	 *
	 * @param array $array
	 *
	 * @return string
	 */

	public static function createPlaceHoldersFromFlattenArray( array $array ): string {
		$placeHolders = str_repeat( '?,', count( $array ) );

		return substr( $placeHolders, 0, -1 );
	}

	/**
	 * returns true if all values of an array are empty
	 *
	 * @param array $array
	 *
	 * @return bool
	 */

	public static function isArrayValuesEmpty( array $array ): bool {
		foreach( ArrayHelper::array_flatten( $array ) as $key => $val ) {
			if ( $val !== '' ) {
				return false;
			}
		}

		return true;
	}
}

